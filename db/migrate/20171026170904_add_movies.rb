class AddMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.integer :genre_id
      t.boolean :featured
      t.date :release_date
      t.integer :director_id

      t.timestamps
    end
  end
end
