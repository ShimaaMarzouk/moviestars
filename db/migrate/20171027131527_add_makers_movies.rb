class AddMakersMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :makers_movies do |t|
      t.integer :maker_id
      t.integer :movie_id

      t.timestamps
    end
  end
end
