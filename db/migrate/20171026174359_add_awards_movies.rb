class AddAwardsMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :awards_movies do |t|
      t.integer :award_id
      t.integer :movie_id
      t.date :acquire_date

      t.timestamps
    end
  end
end
