class AddMoviesRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :movies_ratings do |t|
      t.integer :movie_id
      t.integer :rating_id

      t.timestamps
    end
  end
end
