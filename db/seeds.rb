# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
%w(Action Adventure Comedy Crime Drama Fantasy Historical Horror Mystery Political Romance).each do |genre|
  Genre.create!(:name => genre)
end

%w(G PG PG-13 R NC-17).each do |rating|
  Rating.create!(:name => rating)
end
