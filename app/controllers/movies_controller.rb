class MoviesController < ApplicationController
  # before_filter :authenticate_use

  def new
    @movie = Movie.new
  end

  def create
    @movie = Movie.new(movie_params)
    respond_to do |format|
      if @movie.save
        format.html { redirect_to movies_path, flash_message: "Movie saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @movie = Movie.find(params[:id])
  end

  def update
    @movie = Movie.find(params[:id])
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to movies_path, flash_message: "Movie updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    respond_to do |format|
      format.html { @movies = Movie.all }
    end
  end

  def delete
    @movie = Movie.find(params[:id])
    respond_to do |format|
      if @movie.destroy
        format.html { redirect_to movies_path, flash_message: "Movie deleted successfully" }
      else
        format.html { redirect_to movies_path, flash_message: "Movie can't be deleted" }
      end
    end
  end

  private

  def movie_params
    params.require(:movie).permit(:title, :genre_id, :director_id, :release_date, :featured, actors_attributes: [:name, :_destroy])
  end

end
