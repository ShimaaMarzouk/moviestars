class RatingsController < ApplicationController
  before_filter :authenticate_user

  def new
    @rating = Rating.new
  end

  def create
    @rating = Rating.new(params[:rating])
    respond_to do |format|
      if @rating.save
        format.html { redirect_to ratings_path, flash_message: "Rating saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @rating = Rating.find(params[:id])
  end

  def update
    @rating = Rating.find(params[:id])
    respond_to do |format|
      if @rating.update(params[:rating])
        format.html { redirect_to ratings_path, flash_message: "Rating updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @ratings = Rating.all
  end

  def delete
    @rating = Rating.find(params[:id])
    respond_to do |format|
      if @rating.destroy
        format.html { redirect_to ratings_path, flash_message: "Rating deleted successfully" }
      else
        format.html { redirect_to ratings_path, flash_message: "Rating can't be deleted" }
      end
    end
  end

end
