class NewssController < ApplicationController
  before_filter :authenticate_user

  def new
    @news = News.new
  end

  def create  
    @news = News.new(params[:news])
    respond_to do |format|
      if @news.save
        format.html { redirect_to news_path, flash_message: "News saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @news = News.find(params[:id])
  end

  def update
    @news = News.find(params[:id])
    respond_to do |format|
      if @news.update(params[:news])
        format.html { redirect_to news_path, flash_message: "News updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @news = News.all
  end

  def delete
    @news = News.find(params[:id])
    respond_to do |format|
      if @news.destroy
        format.html { redirect_to news_path, flash_message: "News deleted successfully" }
      else
        format.html { redirect_to news_path, flash_message: "News can't be deleted" }
      end
    end
  end


end
