class DirectorsController < ApplicationController
  before_filter :authenticate_user

  def new
    @director = Director.new
  end

  def create
    @director = Director.new(params[:maker])
    respond_to do |format|
      if @director.save
        format.html { redirect_to directors_path, flash_message: "Director saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @director = Director.find(params[:id])
  end

  def update
    @director = Director.find(params[:id])
    respond_to do |format|
      if @director.update(params[:maker])
        format.html { redirect_to directors_path, flash_message: "Director updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def indexq
    @directors = Director.all
  end

  def delete
    @director = Director.find(params[:id])
    respond_to do |format|
      if @director.destroy
        format.html { redirect_to directors_path, flash_message: "Director deleted successfully" }
      else
        format.html { redirect_to directors_path, flash_message: "Director can't be deleted" }
      end
    end
  end

end
