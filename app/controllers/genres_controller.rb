class GenresController < ApplicationController
  before_filter :authenticate_user

  def new
    @genre = Genre.new
  end

  def create
    @genre = Genre.new(params[:genre])
    respond_to do |format|
      if @genre.save
        format.html { redirect_to genres_path, flash_message: "Genre saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @genre = Genre.find(params[:id])
  end

  def update
    @genre = Genre.find(params[:id])
    respond_to do |format|
      if @genre.update(params[:genre])
        format.html { redirect_to genres_path, flash_message: "Genre updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @genres = Genre.all
  end

  def delete
    @genre = Genre.find(params[:id])
    respond_to do |format|
      if @genre.destroy
        format.html { redirect_to genres_path, flash_message: "Genre deleted successfully" }
      else
        format.html { redirect_to genres_path, flash_message: "Genre can't be deleted" }
      end
    end
  end


end
