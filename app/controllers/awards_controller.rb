class AwardsController < ApplicationController
  before_filter :authenticate_user

  def new
    @award = Award.new
  end

  def create
    @award = Award.new(params[:award])
    respond_to do |format|
      if @award.save
        format.html { redirect_to awards_path, flash_message: "Award saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @award = Award.find(params[:id])
  end

  def update
    @award = Award.find(params[:id])
    respond_to do |format|
      if @award.update(params[:award])
        format.html { redirect_to awards_path, flash_message: "Award updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @awards = Award.all
  end

  def delete
    @award = Award.find(params[:id])
    respond_to do |format|
      if @award.destroy
        format.html { redirect_to awards_path, flash_message: "Award deleted successfully" }
      else
        format.html { redirect_to awards_path, flash_message: "Award can't be deleted" }
      end
    end
  end

end
