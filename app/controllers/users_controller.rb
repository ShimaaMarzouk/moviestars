class UsersController < ApplicationController
  before_filter :authenticate_user

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, flash_message: "User saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(params[:user])
        format.html { redirect_to users_path, flash_message: "User updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @users = User.all
  end

  def delete
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.destroy
        format.html { redirect_to users_path, flash_message: "User deleted successfully" }
      else
        format.html { redirect_to users_path, flash_message: "User can't be deleted" }
      end
    end
  end

end
