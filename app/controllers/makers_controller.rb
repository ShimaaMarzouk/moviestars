class ActorsController < ApplicationController
  before_filter :authenticate_user

  def new
    @actor = Actor.new
  end

  def create
    @actor = Actor.new(params[:maker])
    respond_to do |format|
      if @actor.save
        format.html { redirect_to actors_path, flash_message: "Actor saved successfully" }
      else
        format.html { render action: :new }
      end
    end
  end

  def edit
    @actor = Actor.find(params[:id])
  end

  def update
    @actor = Actor.find(params[:id])
    respond_to do |format|
      if @actor.update(params[:maker])
        format.html { redirect_to actors_path, flash_message: "Actor updated successfully" }
      else
        format.html { render action: :edit }
      end
    end
  end

  def index
    @actors = Actor.all
  end

  def delete
    @actor = Actor.find(params[:id])
    respond_to do |format|
      if @actor.destroy
        format.html { redirect_to actors_path, flash_message: "Actor deleted successfully" }
      else
        format.html { redirect_to actors_path, flash_message: "Actor can't be deleted" }
      end
    end
  end

end
