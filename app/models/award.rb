class Award < ActiveRecord::Base

  ## Validations
  validates :name, presence: true

  ## Relations
  belongs_to :movie

end
