class Actor < Maker

  ## Relations
  has_many :movies, through: :actors_movies
end
