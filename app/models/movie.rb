class Movie < ActiveRecord::Base

  ## Validations
  validates :title, :genre_id, :release_date, :featured, presence: true

  ## Relations
  belongs_to :genre
  has_many :ratings, through: :movies_ratings
  has_many :actors, through: :actors_movies
  belongs_to :director
  has_many :news
  has_many :wish_lists

  ## Scopes
  scope :featured, -> { where(featured: true) }

end
