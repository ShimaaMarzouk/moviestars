class News < ActiveRecord::Base

  ## Validations
  validates :title, :content, presence: true

  ## Relations
  belongs_to :movie

end
