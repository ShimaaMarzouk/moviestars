class Genre < ActiveRecord::Base

  ## Validations
  validates :name, presence: true

  ## Relations
  has_many :movies

end
