class MovieMaker < ActiveRecord::Base

  ## Relations
  has_many :movies
  has_many :makers

end
