class Rating < ActiveRecord::Base

  ## Validations
  validates :name, presence: true

  ## Relations
  has_many :movies

end
